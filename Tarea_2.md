# Tarea 2 
### Paginas 116 - 118
1.  **Analice a Walmart y Amazon.com usando los modelos de fuerzas competitivas y la cadena de valor**

    R//Como fuerza competitiva podemos encontrar que Walmart cuenta con una cadena de proveedores y cliente muy grande que buscan en sus tiendas obtener buenos productos a los mejores precios del mercado, pero a su vez Amazon cuenta con un sistema que ayuda a las personas a realizar compras de manera más fácil.
    Como cadena de valor podemos agregar que Walmart entro en la necesidad de competir de manera digital con Amazon y esta también hizo lo mismo, incursionar en el mundo de tienda físico para dar un plus en sus servicios y llegar a muchos más clientes.

2. **Compare los modelos de negocios y las estrategias de negocios de Walmart y Amazon.**

     R// Los modelos de negocio de estas empresas se basan en casi lo mismo, pero cada uno tiene una forma de llegar a sus clientes en este caso Walmart tiene mas precencia como tienda fisica que Amazon y este tiene mas ventaja como tienda virtual, pero amabas empresas cuenta con estrategia similar que son parte de usar tecnología para temas de marketing y ofertas especiales a sus clientes. 

3. **¿Qué rol desempeña la tecnología de la información en cada uno de estos negocios?**

     R// En Walmart la tecnología lo ayuda para compentir en precio, en saber que productos están en tendencia y poder bajar los precios en tiempo real, como ayudar con sugerencia de productos y artículos de consumo diario.
     Amazon les ayuda a no perder ninguna venta, teniendo un catálogo variable y de que los clientes encuentren siempre lo que están buscan, en Amazon la tecnología es su principal fuerte.

4. **¿Cómo les ayuda a refinar sus estrategias de negocios?**

     R//Los datos de Cada Usuario ayuda a que cada empresa pueda tomar mejores deciciones al momento de realizar una campaña de ṕublicidad o lanzar producto novedoso, tambien en llamar la antencion de presona que nunca pensaron encontrar algun tipo de articulo en alguna de las dos tiendas.

5. **¿Tendrá éxito Walmart contra Amazon.com? Explique su respuesta.**

     R// En pocas palabras Walmart no podrá superar a Amazon en venta en Oline porque Amazon cuenta con una empresa enfocada en el comercio electronico y ya cuenta con una gran cantidad de proveedores y logística que no es facil de superar.